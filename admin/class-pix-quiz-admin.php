<?php

/**
 * The dashboard-specific functionality of the plugin.
 *
 * @link       http://plugins.pixlandmedia.com/pix-quiz
 * @since      1.0.0
 *
 * @package    Pix_Quiz
 * @subpackage Pix_Quiz/admin
 */

/**
 * The dashboard-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the dashboard-specific stylesheet and JavaScript.
 *
 * @package    Pix_Quiz
 * @subpackage Pix_Quiz/admin
 * @author     Tamas Nemeth <nemeth@pixlandmedia.com>
 */
class Pix_Quiz_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $Pix_Quiz    The ID of this plugin.
	 */
	private $Pix_Quiz;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @var      string    $Pix_Quiz       The name of this plugin.
	 * @var      string    $version    The version of this plugin.
	 */
	public function __construct( $Pix_Quiz, $version ) {

		$this->Pix_Quiz = $Pix_Quiz;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the Dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pix_Quiz_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pix_Quiz_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->Pix_Quiz, plugin_dir_url( __FILE__ ) . 'css/pix-quiz-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the dashboard.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Pix_Quiz_Admin_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Pix_Quiz_Admin_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->Pix_Quiz, plugin_dir_url( __FILE__ ) . 'js/pix-quiz-admin.js', array( 'jquery' ), $this->version, false );

	}

}
