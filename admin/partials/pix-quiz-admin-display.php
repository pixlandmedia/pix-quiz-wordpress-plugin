<?php

/**
 * Provide a dashboard view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://plugins.pixlandmedia.com/pix-quiz
 * @since      1.0.0
 *
 * @package    Pix_Quiz
 * @subpackage Pix_Quiz/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
