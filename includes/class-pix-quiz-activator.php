<?php

/**
 * Fired during plugin activation
 *
 * @link       http://plugins.pixlandmedia.com/pix-quiz
 * @since      1.0.0
 *
 * @package    Pix_Quiz
 * @subpackage Pix_Quiz/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Pix_Quiz
 * @subpackage Pix_Quiz/includes
 * @author     Tamas Nemeth <nemeth@pixlandmedia.com>
 */
class Pix_Quiz_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
