<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://plugins.pixlandmedia.com/pix-quiz
 * @since      1.0.0
 *
 * @package    Pix_Quiz
 * @subpackage Pix_Quiz/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Pix_Quiz
 * @subpackage Pix_Quiz/includes
 * @author     Tamas Nemeth <nemeth@pixlandmedia.com>
 */
class Pix_Quiz_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
