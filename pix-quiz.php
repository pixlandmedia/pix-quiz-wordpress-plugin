<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin. http://plugins.pixlandmedia.com/pix-quiz/
 *
 * @link              https://bitbucket.org/pixlandmedia/pix-quiz-wordpress-plugin
 * @since             1.0.5
 * @package           Pix_Quiz
 *
 * @wordpress-plugin
 * Plugin Name:       Pix Quiz temp 
 * Plugin URI:        http://plugins.pixlandmedia.com/pix-quiz/
 * Description:       Quiz Wordpress Plugin
 * Version:           1.0.5
 * Author:            PixLand Media 
 * Author URI:        http://pixlandmedia.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       pix-quiz
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-pix-quiz-activator.php';

/**
 * The code that runs during plugin deactivation.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-pix-quiz-deactivator.php';

/** This action is documented in includes/class-pix-quiz-activator.php */
register_activation_hook( __FILE__, array( 'Pix_Quiz_Activator', 'activate' ) );

/** This action is documented in includes/class-pix-quiz-deactivator.php */
register_deactivation_hook( __FILE__, array( 'Pix_Quiz_Deactivator', 'deactivate' ) );

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-pix-quiz.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_pix_quiz() {

	$plugin = new Pix_Quiz();
	$plugin->run();

}
run_pix_quiz();
